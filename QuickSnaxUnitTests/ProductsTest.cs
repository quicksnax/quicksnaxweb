using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using QuickSnax.Data;
using QuickSnax.Dtos;
using QuickSnax.Models;
using QuickSnax.Repositories;
using Microsoft.AspNetCore.Hosting;
using Moq;
using QuickSnax.Services;
using Xunit;


namespace QuickSnaxUnitTests
{
    public class ProductsTest
    {
        
        [Fact]
        public void GetAllTest()
        {
            var options = new DbContextOptionsBuilder<QuickSnaxContext>().UseInMemoryDatabase("QuickSnax").Options;
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.WebRootPath)
                .Returns("Hosting:UnitTestEnvironment");
            var imageHandler = new ImageUploader(new HttpContextAccessor(),mockEnvironment.Object);
            var context = new QuickSnaxContext(options);

            var services = new ProductsRepository(context,imageHandler);
            Assert.NotNull(services.GetAll());
        }

        [Fact]
        public async Task AddProductTest()
        {
            var options = new DbContextOptionsBuilder<QuickSnaxContext>().UseInMemoryDatabase("QuickSnax").Options;
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.WebRootPath)
                .Returns("Hosting:UnitTestEnvironment");
            var imageHandler = new ImageUploader(new HttpContextAccessor(),mockEnvironment.Object);

            using (var context = new QuickSnaxContext(options))
            {
                var services = new ProductsRepository(context,imageHandler);
            
                var product = new NewProductDto
                {
                    Name = "Squeeze",
                    Price = "120.00",
                    Type = "Soft drink",
                    InStock = "2",
                    ProductImage = null
                };
                Assert.True(await services.Insert(product));    
            }
        }

        [Fact]
        public async Task UpdateProductTest()
        {
            var options = new DbContextOptionsBuilder<QuickSnaxContext>().UseInMemoryDatabase("QuickSnax").Options;
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.WebRootPath)
                .Returns("Hosting:UnitTestEnvironment");
            var imageHandler = new ImageUploader(new HttpContextAccessor(),mockEnvironment.Object);
            using (var context = new QuickSnaxContext(options))
            {
                var services = new ProductsRepository(context,imageHandler);
            
                var product = new NewProductDto
                {
                    Name = "Squeeze",
                    Price = "120.00f",
                    Type = "Soft drink",
                    InStock = "2",
                    ProductImage = null
                };
            
                await services.Insert(product);    
            }

            using (var context = new QuickSnaxContext(options))
            {
                var services = new ProductsRepository(context,imageHandler);

                var updateProduct = services.Find(1).Result;
                updateProduct.Price = 125.00f;
            
                Assert.True(await services.UpdateProduct(updateProduct,null));
                
            }
        }

        [Fact]
        public async Task DeleteProductTest()
        {
            var options = new DbContextOptionsBuilder<QuickSnaxContext>().UseInMemoryDatabase("QuickSnax").Options;
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.WebRootPath)
                .Returns("Hosting:UnitTestEnvironment");
            var imageHandler = new ImageUploader(new HttpContextAccessor(),mockEnvironment.Object);
            using (var context = new QuickSnaxContext(options))
            {
                var services = new ProductsRepository(context,imageHandler);
            
                var product = new NewProductDto
                {
                    Name = "Squeeze",
                    Price = "120.00f",
                    Type = "Soft drink",
                    InStock = "2",
                    ProductImage = null
                };
            
                await services.Insert(product);
            }

            using (var context = new QuickSnaxContext(options))
            {
                var services = new ProductsRepository(context,imageHandler);
                Assert.True(await services.Delete(1));                
            }

        }
    }
}