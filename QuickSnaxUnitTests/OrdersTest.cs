using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Moq;
using QuickSnax.Data;
using QuickSnax.Data.Seeders;
using QuickSnax.Dtos;
using QuickSnax.Models;
using QuickSnax.Services;
using Xunit;

namespace QuickSnaxUnitTests
{
    public class OrdersTest
    {
        [Fact]
        public async Task GetAllOrders()
        {
            var options = new DbContextOptionsBuilder<QuickSnaxContext>().UseInMemoryDatabase("Quicksnax").Options;

            var context = new QuickSnaxContext(options);
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.WebRootPath)
                .Returns("Hosting:UnitTestEnvironment");
            var imageHandler = new ImageUploader(new HttpContextAccessor(),mockEnvironment.Object);

            var services = new OrderRepository(context,imageHandler);

            Assert.NotNull(await services.GetAll());
        }

        /*[Fact]
        public async Task CreateOrderTest()
        {
            var options = new DbContextOptionsBuilder<QuickSnaxContext>().UseInMemoryDatabase("QuickSnax").Options;
            var userId = new Guid().ToString();
            using (var context = new QuickSnaxContext(options))
            {
             
                var services = new OrderRepository(context);
                var product = context.Products.FirstOrDefaultAsync();
                var order = new ProductsOrderDto
                {
                    Location   = "2b7",
                    ProductsOrder = new List<ProductOrderDto>
                    {
                        new ProductOrderDto
                        {
                            ProductId = product.Id,
                            Quantity = 2
                        }
                    }
                };
                Assert.True(await services.MakeOrder(order,userId));    
            }*/
        }
    }
