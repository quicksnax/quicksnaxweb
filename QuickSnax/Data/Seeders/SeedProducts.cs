using System;
using System.Threading.Tasks;
using Bogus;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using QuickSnax.Models;

namespace QuickSnax.Data.Seeders
{
    public static class SeedProducts
    {
        private static QuickSnaxContext _context;

        public static async Task Init(QuickSnaxContext context)
        {
            _context = context;
            await CreateProducts();
        }

        private static async Task CreateProducts()
        {
            var productsExist = _context.Products.Any();
            var shop = await _context.Shops.FirstOrDefaultAsync();
            
            if (!productsExist)
            {
                var productRules = new Faker<Product>()
                    .RuleFor(product => product.Name, faker => faker.Commerce.Product())
                    .RuleFor(product => product.ShopId, shop.Id)
                    .RuleFor(product => product.Type, faker => faker.Commerce.ProductMaterial())
                    .RuleFor(product => product.Price, faker => float.Parse(faker.Commerce.Price()))
                    .RuleFor(product => product.InStock, faker => faker.Random.Number(15, 50))
                    .RuleFor(product => product.ImageUrl, faker => faker.Image.PicsumUrl())
                    .RuleFor(product => product.CreatedAt, DateTime.Now)
                    .RuleFor(product => product.UpdatedAt, DateTime.Now);

                var products = productRules.Generate(30);
                
                await _context.AddRangeAsync(products);
                await _context.SaveChangesAsync();
            }
        }
    }
}