using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace QuickSnax.Data.Seeders
{
    public static class SeedRoles
    {
        private static RoleManager<IdentityRole> _roleManager;

        public static async Task Init(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
            await CreateRoles();
        }

        private static async Task CreateRoles()
        {
            if (!await _roleManager.RoleExistsAsync(Constants.RoleNames.ShopOwner))
            {
                await _roleManager.CreateAsync(new IdentityRole(Constants.RoleNames.ShopOwner));
            }
            
            if (!await _roleManager.RoleExistsAsync(Constants.RoleNames.DeliveryPerson))
            {
                await _roleManager.CreateAsync(new IdentityRole(Constants.RoleNames.DeliveryPerson));
            }
            
            if (!await _roleManager.RoleExistsAsync(Constants.RoleNames.Student))
            {
                await _roleManager.CreateAsync(new IdentityRole(Constants.RoleNames.Student));
            }
        }        
    }
}