using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using QuickSnax.Models;

namespace QuickSnax.Data.Seeders
{
    public static class Seeder
    {
        public static async Task InitiateDb(IServiceProvider services)
        {
            var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
            var context = services.GetRequiredService<QuickSnaxContext>();
            var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

            await SeedRoles.Init(roleManager);
            await SeedUser.Init(userManager, context);
            await SeedProducts.Init(context);
            await SeedOrders.Init(context);
        }
    }
}