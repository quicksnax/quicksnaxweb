using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Microsoft.EntityFrameworkCore;
using QuickSnax.Models;

namespace QuickSnax.Data.Seeders
{
    public static class SeedOrders
    {
        private static QuickSnaxContext _context;

        public static async Task Init(QuickSnaxContext context)
        {
            _context = context;
            await MakeOrders();
        }

        private static async Task MakeOrders()
        {
            var products = _context.Products.Select(x => x.Id).ToListAsync().Result;

            var students = await _context.Students.Select( x=> x.Id).ToListAsync();
            var ordersExist = _context.Orders.Any();
            if (!ordersExist)
            {
                if (products.Count > 0 && students.Count > 0)
                {
                    var productOrderRules = new Faker<ProductOrder>()
                        .RuleFor(order => order.ProductId, faker => faker.PickRandom(products))
                        .RuleFor(order => order.Quantity, faker => faker.Random.Number(2, 5))
                        .RuleFor(order => order.OrderId, faker => new Order().Id);

                    var orderRules = new Faker<Order>()
                        .RuleFor(order => order.Status, "Undelivered")
                        .RuleFor(order => order.Location, faker => faker.Address.City())
                        .RuleFor(order => order.StudentId, faker => faker.PickRandom(students))
                        .RuleFor(order => order.CreatedAt, DateTime.Now)
                        .RuleFor(order => order.UpdatedAt, DateTime.Now)
                        .RuleFor(order => order.ProductOrders, faker => productOrderRules.Generate(2).ToList());

                    var orders = orderRules.Generate(15);

                    foreach (var order in orders)
                    {
                        await _context.Orders.AddAsync(order);
                        await _context.ProductOrders.AddRangeAsync(order.ProductOrders);
                    }

                    await _context.SaveChangesAsync();
                }
            }
        }
        
    }
}