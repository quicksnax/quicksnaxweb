using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using QuickSnax.Models;

namespace QuickSnax.Data.Seeders
{
    public static class SeedUser
    {
        private static UserManager<ApplicationUser> _userManager;
        private static QuickSnaxContext _context;

        public static async Task Init(UserManager<ApplicationUser> userManager, QuickSnaxContext context)
        {
            _context = context;
            _userManager = userManager;
            await CreateShopOwnerAsync();
            await CreateDeliveryAsync();
            await CreateStudentsAsync();
        }

        private static async Task CreateShopOwnerAsync()
        {
            var ownerUser = await _userManager.Users
                .Where(x => x.UserName == "owner@snax.com")
                .SingleOrDefaultAsync();


            if (ownerUser != null) return;

            ownerUser = new ApplicationUser
            {
                UserName = "owner@snax.com",
                FirstName = "John",
                LastName = "Brown",
                Email = "owner@snax.com",
                EmailConfirmed = true
            };

            
            var result = await _userManager.CreateAsync(
                ownerUser, "Owner101!");
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(ownerUser, Constants.RoleNames.ShopOwner);


                var owner = new Owner
                {
                    UserId = ownerUser.Id
                };
                _context.Owners.Add(owner);
                var ownerResult = await _context.SaveChangesAsync();
                if (ownerResult > 0)
                {
                    var shop = new Shop
                    {
                        Name = "John shop",
                        OwnerId = owner.Id
                    };
                    _context.Shops.Add(shop);
                    await _context.SaveChangesAsync();
                }

            }
        }

        private static async Task CreateDeliveryAsync()
        {
            var delivery = await _userManager.Users
                .Where(x => x.UserName == "delivery@snax.com")
                .SingleOrDefaultAsync();


            if (delivery != null) return;

            delivery = new ApplicationUser
            {
                UserName = "delivery@snax.com",
                FirstName = "Some",
                LastName = "Rider",
                Email = "delivery@snax.com",
                EmailConfirmed = true
            };

            await _userManager.CreateAsync(
                delivery, "Delivery101!");

            await _userManager.AddToRoleAsync(delivery, Constants.RoleNames.DeliveryPerson);
        }

        private static async Task CreateStudentsAsync()
        {
            var exists = await _userManager.GetUsersInRoleAsync(Constants.RoleNames.Student);

            if (!exists.Any())
            {
                var studentRules = new Faker<Student>()
                    .RuleFor(student => student.IdNumber, faker => faker.Phone.PhoneNumber("#######"));

                var users = studentRules.Generate(10);
                foreach (var student in users)
                {
                    student.UserId = await CreateStudent();
                    _context.Students.Add(student);
                }

                await _context.SaveChangesAsync();
            }
        }

        private static async Task<string> CreateStudent()
        {
            var exists = await _userManager.GetUsersInRoleAsync(Constants.RoleNames.Student);

            if (exists.Count < 11)
            {
                var userRules = new Faker<ApplicationUser>()
                    .RuleFor(user => user.FirstName, faker => faker.Person.FirstName)
                    .RuleFor(user => user.LastName, faker => faker.Person.LastName)
                    .RuleFor(user => user.UserName, faker => faker.Person.UserName)
                    .RuleFor(user => user.Email, faker => faker.Person.Email)
                    .RuleFor(user => user.EmailConfirmed, true);

                var student = userRules.Generate();
                var result = await _userManager.CreateAsync(student, "Student101!");
                if (result.Succeeded)
                {
                    var roleResult = await _userManager.AddToRoleAsync(student, Constants.RoleNames.Student);
                    if (roleResult.Succeeded)
                    {
                        return student.Id;
                    }
                }
            }

            return new ApplicationUser().Id;
        }
    }

}