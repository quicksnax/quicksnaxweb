using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using QuickSnax.Models;

namespace QuickSnax.Data
{
    public class UserClaims : UserClaimsPrincipalFactory<ApplicationUser,IdentityRole>
    {
        public UserClaims(UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager, IOptions<IdentityOptions> options) 
            : base(userManager, roleManager, options)
        {
            
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
        {
            var identity = await base.GenerateClaimsAsync(user);
            identity.AddClaim(new Claim("FirstName",user.FirstName));
            identity.AddClaim(new Claim("LastName",user.LastName));
            return identity;
        }
    }
}