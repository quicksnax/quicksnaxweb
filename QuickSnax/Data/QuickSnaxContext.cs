using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using QuickSnax.Models;


namespace QuickSnax.Data
{
    public class QuickSnaxContext : IdentityDbContext<ApplicationUser>
    {
        public QuickSnaxContext(DbContextOptions<QuickSnaxContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            builder.Entity<ProductOrder>().HasKey(orderProduct => new {orderProduct.ProductId, orderProduct.OrderId});

            builder.Entity<ProductOrder>()
                .HasOne(x => x.Order)
                .WithMany(x => x.ProductOrders)
                .HasForeignKey(x => x.OrderId);

            builder.Entity<ProductOrder>()
                .HasOne(x => x.Product)
                .WithMany(x => x.ProductOrders)
                .HasForeignKey(x => x.ProductId);
            
            base.OnModelCreating(builder);
            
        }


        public DbSet<Delivery> Deliveries { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Rider> Riders { get; set; }

        public DbSet<Shop> Shops { get; set; }

        public DbSet<Owner> Owners { get; set; }

        public DbSet<ProductOrder> ProductOrders { get; set; }

        public DbSet<Student> Students { get; set; }
        
        // public override int SaveChanges()
        // {
        //     AddTimestamps();
        //     return base.SaveChanges();
        // }

        // public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken= new CancellationToken())
        // {
        //     AddTimestamps();
        //     return await base.SaveChangesAsync();
        // }
        
        // private void AddTimestamps()
        // {
        //     var entities = ChangeTracker.Entries()
        //         .Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

        //     foreach (var entity in entities)
        //     {
        //         var now = DateTime.UtcNow; // current datetime

        //         if (entity.State == EntityState.Added)
        //         {
        //             ((BaseEntity)entity.Entity).CreatedAt = now;
        //         }
        //         ((BaseEntity)entity.Entity).UpdatedAt = now;
        //     }
        // }
        
    }
}