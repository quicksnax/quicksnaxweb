namespace QuickSnax
{
    public static class Constants
    {
        public static class RoleNames
        {
            public const string ShopOwner = "ShopOwner";

            public const string DeliveryPerson = "DeliveryPerson";

            public const string Student = "Student";
        }
        
        public static class OrderStatus
        {
            public const string UnDelivered = "Undelivered";

            public const string InProgress = "In progress";

            public const string Delivered = "Delivered";
        }

        
    }
}