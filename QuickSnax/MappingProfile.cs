using System.Collections.Generic;
using AutoMapper;
using QuickSnax.Dtos;
using QuickSnax.Models;

namespace QuickSnax
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>();
            CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>();
            CreateMap<ProductOrderDto,IList<ProductOrder>>();
            CreateMap<IList<ProductOrder>, ProductOrderDto>();
        }
    }
}