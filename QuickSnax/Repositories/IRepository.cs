using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuickSnax.Repositories
{
    public interface IRepository
    {
        
        Task<bool> Delete(int id);
    }
}