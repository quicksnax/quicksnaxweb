using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using QuickSnax.Dtos;
using QuickSnax.Models;

namespace QuickSnax.Repositories
{
    public interface IProductsRepository: IRepository
    {
        Task<IEnumerable<ProductDto>> GetAll();
        Task<bool> Insert(NewProductDto product);
        Task<bool> UpdateProduct(Product product,IFormFile image);

        Task<Product> Find(int id);

        Task<IEnumerable<Product>> Find(string name);
        
    }
}