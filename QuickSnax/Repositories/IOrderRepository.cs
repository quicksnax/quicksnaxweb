using System.Collections.Generic;
using System.Threading.Tasks;
using QuickSnax.Dtos;
using QuickSnax.Models;

namespace QuickSnax.Repositories
{
    public interface IOrderRepository: IRepository
    {
        Task<List<OrderDateDto>> GetAll();
        
        Task<bool> MakeOrder(ProductsOrderDto orders,string userId);

        Task<StudentOrderDto> GetStudentOrder(int id);
        

        Task<bool> UpdateOrderStatus(int id, string status);

        Task<List<PreviousOrdersDto>> GetPreviousOrders(string userId);

    }
}