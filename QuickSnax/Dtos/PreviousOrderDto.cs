using System;
using System.Collections.Generic;

namespace QuickSnax.Dtos
{
    public class PreviousOrdersDto
    {
        public String OrderDate { get; set; }

        public List<PreviousOrderDto> PreviousOrders { get; set; }
    }

    public class PreviousOrderDto
    {
        public int OrderId { get; set; }

        public float OrderTotal { get; set; }

        public string OrderStatus { get; set; }

        public int ProductAmount { get; set; }
        
        public string Location { get; set; }

        public string TimeOfOrder { get; set; }
    }
}