using System.Collections.Generic;

namespace QuickSnax.Dtos
{
    public class ProductsOrderDto
    {
        public List<ProductOrderDto> ProductsOrder { get; set; } //change this to list

        public string Location { get; set; }
    }
    
    public class ProductOrderDto
    {
        public int ProductId { get; set; }
            
        public int Quantity { get; set; }
    }
}