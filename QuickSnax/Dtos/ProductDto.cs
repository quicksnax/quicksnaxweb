
namespace QuickSnax.Dtos
{
 public class ProductDto
   {
      public int Id { get; set; }

      public string Name { get; set; }

      public float Price { get; set; }

      public string Type { get; set; }

      public int InStock { get; set; }

      public string ImageUrl { get; set; }   
   }
   
   
   
}