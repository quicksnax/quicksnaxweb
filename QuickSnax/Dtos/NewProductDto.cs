using Microsoft.AspNetCore.Http;

namespace QuickSnax.Dtos
{
    public class NewProductDto
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }

        public string InStock { get; set; }
        
        public IFormFile ProductImage { get; set; }
        
    }
}