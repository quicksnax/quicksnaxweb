using System;
using System.Collections.Generic;

namespace QuickSnax.Dtos
{

    public class OrderDateDto
    {
        public string OrderDate { get; set; }

        public List<OrderDto> Orders { get; set; }
    }
    public class OrderDto
    {
        public int OrderId { get; set; }
        
        public string StudentName { get; set; }

        public string Location { get; set; }

        public float Total { get; set; }    
        public int ItemCount { get; set; }    
        public string OrderStatus { get; set; }
        public string TimeOfOrder { get; set; }
    }

}
