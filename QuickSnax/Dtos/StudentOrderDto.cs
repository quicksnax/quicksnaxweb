using System.Collections.Generic;

namespace QuickSnax.Dtos
{
    public class StudentOrderDto
    {
        public int OrderId { get; set; }

        public string StudentName { get; set; }

        public string IdNumber { get; set; }

        public string Location { get; set; }

        public float Total { get; set; }

        public List<StudentProductDto> Products { get; set; }
    }

    public class StudentProductDto
    {
        public int Id { get; set; }
        
        public float Price { get; set; }

        public string ImageUrl { get; set; }

        public int Quantity { get; set; }

        public string ProductName { get; set; }
        
    }
}