using IdentityModel.Client;
using QuickSnax.Models;

namespace QuickSnax.Dtos
{
    public class UserInfoDto
    {
        
        public string UserName { get; set; }

        public string Token { get; set; }

        public UserInfoDto()
        {
            
        }

        public UserInfoDto(string UserName, string token)
        {
            this.UserName = UserName;
            Token = token;
        }    
        
    }
    
   
}