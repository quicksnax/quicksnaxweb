using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickSnax.Models
{
    public class Shop
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int OwnerId { get; set; }
        
        [ForeignKey("OwnerId")]
        public Owner Owner { get; set; }
        
        [Timestamp]
        public byte[] RowVersion { get; set; }
        
    }
    
}