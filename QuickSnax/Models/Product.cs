using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickSnax.Models
{
    public class Product : BaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public string Type { get; set; }

        public float Price { get; set; }

        public int InStock { get; set; }

        public string ImageUrl { get; set; }
        
        public int ShopId { get; set; }    

        [ForeignKey("ShopId")]
        public Shop Shop { get; set; }

        public virtual IList<ProductOrder> ProductOrders { get; set; }

        public override DateTime CreatedAt
        {
            get
            {
                return DateTime.Now;
            }
        }
        public override DateTime UpdatedAt
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}