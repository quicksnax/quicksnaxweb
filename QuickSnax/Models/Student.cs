using System.ComponentModel.DataAnnotations.Schema;

namespace QuickSnax.Models
{
    public class Student
    {
        public int Id { get; set; }

        public string IdNumber { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }
}