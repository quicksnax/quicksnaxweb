using System;

namespace QuickSnax.Models
{
    public abstract class BaseEntity
    {
        public abstract DateTime CreatedAt { get; }

        public abstract DateTime UpdatedAt { get; }
    }
}