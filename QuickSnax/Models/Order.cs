﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSnax.Models
{
    public class Order : BaseEntity
    {
        public int Id { get; set; }

        public float Total { get; set; }

        public string Status { get; set; }

        public string Location { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        public override DateTime CreatedAt
        {
            get
            {
                return DateTime.Now;
            }
        }
        public override DateTime UpdatedAt
        {
            get
            {
                return DateTime.Now;
            }
        }
        
        public virtual IList<ProductOrder> ProductOrders { get; set; }
    }
}