using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickSnax.Models
{
    public class Owner
    {
        public int Id { get; set; }
    
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }
        
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}