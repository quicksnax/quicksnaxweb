using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickSnax.Models
{
    public class Rider
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string VehicleType { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }
        
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}