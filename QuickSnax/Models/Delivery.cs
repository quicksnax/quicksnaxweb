using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickSnax.Models
{
    public class Delivery : BaseEntity
    {
        public int Id { get; set; }
        
        public int OrderId { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CompletedAt { get; set; }
        
        [ForeignKey("OrderId")]
        public Order Order { get; set; }

        public int RiderId { get; set; }

        [ForeignKey("RiderId")]
        public Rider Rider { get; set; }
        
        public override DateTime CreatedAt
        {
            get
            {
                return DateTime.Now;
            }
        }
        public override DateTime UpdatedAt
        {
            get
            {
                return DateTime.Now;
            }
        }
        
    }
}