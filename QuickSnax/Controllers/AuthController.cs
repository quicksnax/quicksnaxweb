using System;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using QuickSnax.Auth;
using QuickSnax.Dtos;
using QuickSnax.Models;
using static IdentityModel.Client.DiscoveryClient;

namespace QuickSnax.Controllers
{
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        

        public AuthController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        
        }

        [HttpPost("refreshtoken")]
        public async Task<IActionResult> RefreshToken(string refreshToken)
        {
            try
            {
                var identityService = await GetAsync("http://localhost:5000");
                var tokenClient = new HttpClient();
                var response = await tokenClient.RequestRefreshTokenAsync(new RefreshTokenRequest
                {
                    ClientId = "app",
                    ClientSecret = "QuickSnax2019",
                    Address = identityService.TokenEndpoint,
                    RefreshToken = refreshToken
                });
                return Ok(new {tokenResponse = response});
            }
            catch (Exception ex)
            {
                return Ok(new { success = false, data = string.Empty, message = ex.Message });
            }
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]UserDto userInfo)
        {
            var client = new HttpClient();
            
            userInfo.UserName = userInfo.UserName.Trim();

            var response = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = "http://localhost:5000/connect/token",
                GrantType = "password",
                ClientId = "app",
                ClientSecret = "QuickSnax2019",
                Scope = "api1 openid",
                UserName = userInfo.UserName,
                Password = userInfo.Password
            });
            
            if (response.IsError)
            {
                return BadRequest(response.Error);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = await _userManager.FindByNameAsync(userInfo.UserName);

            if (user != null && await _userManager.CheckPasswordAsync(user, userInfo.Password))
            {
                return Ok(new UserInfoDto(user.UserName,response.AccessToken));
            }

            return BadRequest("Invalid username or password");
        }
    }
}