using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using QuickSnax.Dtos;
using QuickSnax.Models;
using QuickSnax.Repositories;

namespace QuickSnax.Controllers
{
    [Route("api/order")]
    [ApiController]
    [Authorize]
    public class OrdersController : ControllerBase
    {    
        
        private readonly IOrderRepository _ordersRepository;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public OrdersController(IOrderRepository ordersRepository, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _ordersRepository = ordersRepository;
            _mapper = mapper;
            _userManager = userManager;
        }
        
        [Authorize(Roles = Constants.RoleNames.ShopOwner+","+Constants.RoleNames.DeliveryPerson)]
        [HttpGet("list")]
        public async Task<IActionResult> List()
        {
            return Ok(await _ordersRepository.GetAll());
        }

        [HttpPost]
        [Authorize(Roles = Constants.RoleNames.Student)]
        public async Task<IActionResult> Create(ProductsOrderDto orders)
        {
            var user = User;

            if (user.IsInRole(Constants.RoleNames.Student))
            {
                var userId = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                                
                if (orders == null || !ModelState.IsValid) return BadRequest();
                var result = await _ordersRepository.MakeOrder(orders, userId);
                if (result)
                {
                    return Ok("Your order has been submitted");    
                }
                return BadRequest("Oh no your order could not be processed");
                
            }

            return Unauthorized("You are not authorized to make an order");
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetStudentOrder(int id)
        {
            var student = await _ordersRepository.GetStudentOrder(id);
            return Ok(student);
        }
        
        [HttpPost("update/{id}")]
        [Authorize(Roles = Constants.RoleNames.ShopOwner+","+Constants.RoleNames.DeliveryPerson)]
        public async Task<IActionResult> UpdateOrderStatus(int id, [FromBody]string status)
        {
            if ((id != null || id != 0) && !status.IsNullOrEmpty())
            {
                var result =await _ordersRepository.UpdateOrderStatus(id, status);
                if (result)
                {
                    return Ok("Status updated");
                }
                return BadRequest("Order status cannot be changed");
            }

            return BadRequest("Oh no! Something happened");

        }

        [HttpDelete("{id}")]
        [Authorize(Roles = Constants.RoleNames.Student)]
        public async Task<IActionResult> Delete(int id)
        {
            
            return Ok(await _ordersRepository.Delete(id));
        }

        [HttpGet("previous")]
        [Authorize(Roles = Constants.RoleNames.Student)]
        public async Task<IActionResult> GetPreviousOrders()
        {
            var user = User;

            if (user.IsInRole(Constants.RoleNames.Student))
            {
               var userId = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
               if (userId != null)
               {
                   return Ok(await _ordersRepository.GetPreviousOrders(userId));
               }
            }
            return new BadRequestObjectResult("YOU DO NOT HAVE ANY ORDERS");
        }
    }
}