using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using QuickSnax.Dtos;
using QuickSnax.Models;
using QuickSnax.Repositories;

namespace QuickSnax.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {

        private readonly IProductsRepository _productsRepository;

        public ProductsController(IProductsRepository productsRepository)
        {
            _productsRepository = productsRepository;
        }

        
        [Authorize]
        [HttpGet("list")]
        public async Task<IActionResult> List()
        { 
            return Ok(await _productsRepository.GetAll());
        }

        
        [HttpPost]
        [Authorize(Roles = Constants.RoleNames.ShopOwner)]
        public async Task<IActionResult> Create([FromForm]NewProductDto product)
        {
            var user = HttpContext.User;
            if (user.IsInRole(Constants.RoleNames.ShopOwner))
            {
                try
                {
                    if (product == null || !ModelState.IsValid)
                    {
                        return BadRequest("Model invalid");
                    }

                    if (await _productsRepository.Insert(product))
                    {
                        return Ok("Add product"); 
                    }
                }
                catch (Exception e)
                {
                    return BadRequest("Could not add product");
                }
                return BadRequest("Something happened");
            }

            return Unauthorized("You are not authorized for this action");
        }

        [HttpPut("{id}")]
        [Authorize(Roles = Constants.RoleNames.ShopOwner)]
        public async Task<IActionResult> Edit(int id,[FromForm]NewProductDto product)
        {
            try
            {
                if (product == null || !ModelState.IsValid)
                {
                    return BadRequest("Something happened");
                }

                var existingProduct = _productsRepository.Find(id).Result;
                if (existingProduct == null)
                {
                    return NotFound("Could not find product");
                }

                existingProduct.Type = product.Type;
                existingProduct.Name = product.Name;
                existingProduct.InStock = Int32.TryParse(product.InStock,out int stock)? stock : 0;
                existingProduct.Price = float.TryParse(product.Price, out float price) ? price : 0.00f; 
               
                await _productsRepository.UpdateProduct(existingProduct,product.ProductImage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return Ok("Updated product");
        }

        [Authorize(Roles = Constants.RoleNames.ShopOwner)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest("ID not valid");
            }

            if (await _productsRepository.Delete(id))
            {
                return Ok("Product was successfully deleted");
            }
            return BadRequest("Product could not be deleted");
        }
        
    }
}