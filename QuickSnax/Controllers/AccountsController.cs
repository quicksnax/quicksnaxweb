using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using QuickSnax.Data;
using QuickSnax.Dtos;
using QuickSnax.Models;

namespace QuickSnax.Controllers
{
    
    [Route("api/account")]
    public class AccountsController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly QuickSnaxContext _context;
        
        public AccountsController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, QuickSnaxContext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegistrationDto registrationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = new ApplicationUser
                {
                    FirstName = registrationDto.FirstName,
                    LastName = registrationDto.LastName,
                    Email = registrationDto.Email,
                    UserName = registrationDto.Email,
                    EmailConfirmed = true
                };
                var result = await _userManager.CreateAsync(user, registrationDto.Password);
                string role = Constants.RoleNames.Student;

                if (!result.Succeeded) return new BadRequestObjectResult(result.Errors);

                if (await _roleManager.FindByNameAsync(role) == null)
                {
                    await _roleManager.CreateAsync(new IdentityRole(role));
                }

                await _userManager.AddToRoleAsync(user, role);
                await _userManager.AddClaimAsync(user, new Claim("username", user.UserName));
                await _userManager.AddClaimAsync(user, new Claim("email", user.Email));
                await _userManager.AddClaimAsync(user, new Claim("role", role));

                var student = new Student
                {
                    UserId = user.Id,
                    IdNumber = registrationDto.IdNumber
                };

                _context.Students.Add(student);
                var studentResult = await _context.SaveChangesAsync();

                if (studentResult != 1) return new BadRequestObjectResult("Unable to create student");

                return new OkObjectResult(new UserInfoDto(user.UserName, ""));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new BadRequestObjectResult("Unable to create user");
            }
        }
    }
}