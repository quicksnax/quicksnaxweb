using System;
using System.IO;
using System.Net;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using QuickSnax.Extensions;
using QuickSnax.Repositories;

namespace QuickSnax.Services
{
    public class ImageUploader
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ImageUploader(IHttpContextAccessor contextAccessor, IHostingEnvironment hostingEnvironment)
        {
            _contextAccessor = contextAccessor;
            _hostingEnvironment = hostingEnvironment;
        }

        public String GetImage(String name)
        {
            var productName = name.Replace(" ","");
            var productImageUrl = "";
            var request = _contextAccessor.HttpContext.Request;
            var host = request.Host;
            string webRoot = _hostingEnvironment.WebRootPath;
            if (name != null)
            {
                if (Directory.Exists(webRoot + "/images/"))
                {

                    if (Directory.Exists(webRoot + "/images/product-images/")
                    ) //checks to ensure that the Profile Pictures folder exists
                    {
                        productImageUrl = Path.Combine("http://" + host + "/images/product-images/",
                            productName + ".jpg");
                        return productImageUrl;

                    }
                }
            }
            return productImageUrl;

        }
        
        
        public async Task<String> UploadImage(String name,IFormFile productImage)
        {
            var productName = name.Replace(" ","");
            var imageFileName = "";
            if (productImage.IsImage())
            {
                try
                {
                    var webRoot = _hostingEnvironment.WebRootPath; //sets the current public folder
                    // checks to see if Profile Pictures is already directory in wwwwroot

                    if (!Directory.Exists(webRoot + "/images/"))
                    {
                        //if it isn't then it creates it
                        Directory.CreateDirectory(webRoot + "/images/");
                    }

                    if (!Directory.Exists(webRoot + "/images/product-images/"))
                    {
                        //if it isn't then it creates it
                        Directory.CreateDirectory(webRoot + "/images/product-images/");
                    }
                    
                    // basically concatenates the webroot folder and the image name
                    var fileName = Path.Combine(webRoot + "/images/product-images/" , productName+".jpg");

                    //it creates a stream meaning it's a buffer to basically write to somewhere permanent(might be wrong tho it's 1:45am)
                    using (var stream = new FileStream(fileName, FileMode.Create))
                    {
                        await productImage.CopyToAsync(stream);
                        imageFileName = "/images/product-images/" + productName + ".jpg";
                        return imageFileName;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            return imageFileName;
        }
    }
}