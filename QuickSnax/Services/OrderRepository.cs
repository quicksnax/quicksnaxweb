using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml.Linq;
using Bogus.Extensions;
using Microsoft.EntityFrameworkCore;
using QuickSnax.Data;
using QuickSnax.Dtos;
using QuickSnax.Models;
using QuickSnax.Repositories;

namespace QuickSnax.Services
{
    public class OrderRepository : IOrderRepository
    {
        
        private readonly QuickSnaxContext _quickSnaxContext;
        private readonly ImageUploader _imageHandler;

        public OrderRepository(QuickSnaxContext quickSnaxContext, ImageUploader imageHandler)
        {
            _quickSnaxContext = quickSnaxContext;
            _imageHandler = imageHandler;
        }
        
        public async Task<List<OrderDateDto>> GetAll()
        {
            return await _quickSnaxContext.Orders
                .GroupBy(i => i.CreatedAt.Date,(key,group) => new OrderDateDto
                {
                    OrderDate = key.ToShortDateString(),
                    Orders = group.Select(x => new OrderDto
                    {
                        OrderId = x.Id,
                        Location = x.Location,
                        OrderStatus = x.Status,
                        Total = x.ProductOrders.Sum(y => y.Quantity* y.Product.Price),
                        ItemCount = x.ProductOrders.Count,
                        StudentName = x.Student.ApplicationUser.FirstName +" " + x.Student.ApplicationUser.LastName,
                        TimeOfOrder = x.CreatedAt.ToShortDateString()
                    }).ToList()
                }).OrderByDescending(dto => dto.OrderDate)
                .ToListAsync();
                
        }

        public async Task<bool> MakeOrder(ProductsOrderDto orders,string userId)
        {
            var student = await _quickSnaxContext.Students.FirstOrDefaultAsync(x => x.UserId == userId);

            if (student != null)
            {
                var order = new Order
                {
                    Location = orders.Location,
                    StudentId = student.Id,
                    Status = Constants.OrderStatus.UnDelivered
                };
        
                var productOrders = orders.ProductsOrder.Select(x =>
                    new ProductOrder
                    {
                        OrderId = order.Id,
                        ProductId = x.ProductId,
                        Quantity = x.Quantity
                    }
                ).ToList();
    
                order.ProductOrders = productOrders;
                
                lock (productOrders)
                {
                    foreach (var productOrder in productOrders)
                    {
                        var product = _quickSnaxContext.Products.Find(productOrder.ProductId);

                        if (productOrder.Quantity <= product.InStock)
                        {
                            product.InStock -= productOrder.Quantity;

                            _quickSnaxContext.Entry(product).State = EntityState.Modified;

                            _quickSnaxContext.SaveChanges();
                        }
                        else
                        {
                            return false;
                        }
                    }    
                }
                await _quickSnaxContext.Orders.AddAsync(order);
                await _quickSnaxContext.ProductOrders.AddRangeAsync(order.ProductOrders);
                return await _quickSnaxContext.SaveChangesAsync() > 0 ;
            }
            return false;
        }

        public async Task<bool> UpdateOrderStatus(int id,string status)
        {
            var order =await _quickSnaxContext.Orders.FirstOrDefaultAsync(x => x.Id == id);
            order.Status = status;
            _quickSnaxContext.Entry(order).State = EntityState.Modified;
            return await _quickSnaxContext.SaveChangesAsync() == 1; 
        }

        public async Task<StudentOrderDto> GetStudentOrder(int id)
        {
            var orders = await _quickSnaxContext.Orders
                .Where(x => x.Id == id)
                .Select(x => new StudentOrderDto
                {
                    OrderId = x.Id,
                    Location = x.Location,
                    Total = x.Total,
                    StudentName = x.Student.ApplicationUser.FirstName + " " + x.Student.ApplicationUser.LastName,
                    IdNumber = x.Student.IdNumber,
                    Products = x.ProductOrders.Join(_quickSnaxContext.Products, 
                    order => order.ProductId, product => product.Id, (order, product) => new {order,product})
                    .Select(y => new StudentProductDto
                    {
                        Id = y.product.Id,
                        Price = y.product.Price,     
                        Quantity = y.order.Quantity,
                        ImageUrl = !y.product.ImageUrl.Contains("http")? _imageHandler.GetImage(y.product.Name):y.product.ImageUrl,
                        ProductName = y.product.Name
                    }).ToList()
                }).FirstOrDefaultAsync();   
            return orders;
        }

        public async Task<bool> Delete(int id)
        {
            var order = _quickSnaxContext.Orders.FirstOrDefault(x=> x.Id == id);
            if (order == null)
            {
                return false;
            }
            _quickSnaxContext.Orders.Remove(order);

            return await _quickSnaxContext.SaveChangesAsync() > 0 ;
            
        }

        public async Task<List<PreviousOrdersDto>> GetPreviousOrders(string userId)
        {
            var orders = await _quickSnaxContext.Orders
                .OrderByDescending(x => x.CreatedAt.Date)
                .Where(x => x.Student.UserId == userId)
                .GroupBy(x => x.CreatedAt, (key, group) => new PreviousOrdersDto
                {
                    OrderDate = key.ToShortDateString(),
                    PreviousOrders = group.Select(x => new PreviousOrderDto
                    {
                        OrderId = x.Id,
                        OrderTotal = x.ProductOrders.Sum(y => y.Quantity* y.Product.Price),
                        Location = x.Location,
                        OrderStatus = x.Status,
                        ProductAmount = x.ProductOrders.Count,
                        TimeOfOrder = x.CreatedAt.ToShortDateString()
                    }).ToList()
                }).OrderByDescending(dto => dto.OrderDate)
                .ToListAsync();
            return orders;
        }
    }
}
