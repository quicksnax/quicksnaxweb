using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using QuickSnax.Data;
using QuickSnax.Dtos;
using QuickSnax.Models;
using QuickSnax.Repositories;

namespace QuickSnax.Services
{
    public class ProductsRepository : IProductsRepository
    {
        private readonly QuickSnaxContext _quickSnaxContext;
        private readonly ImageUploader _imageHandler;

        public ProductsRepository(QuickSnaxContext quickSnaxContext, ImageUploader imageHandler)
        {
            _quickSnaxContext = quickSnaxContext;
            _imageHandler = imageHandler;
        }

        public async Task<IEnumerable<ProductDto>> GetAll()
        {
            var products = await _quickSnaxContext.Products.Select(x => new ProductDto
            {
                Id = x.Id,
                Name = x.Name,
                Price = x.Price,
                Type = x.Type,
                InStock = x.InStock,
                ImageUrl = !x.ImageUrl.Contains("http")? _imageHandler.GetImage(x.Name):x.ImageUrl,
            }).ToListAsync();

            return products;
        }

        public async Task<bool> Insert(NewProductDto product)
        {
            using (_quickSnaxContext)
            {
                try
                {
                
                    var newProduct = new Product
                    {
                        Name = product.Name,
                        InStock = Int32.TryParse(product.InStock,out int stock)? stock : 0,
                        Price = float.TryParse(product.Price, out float price) ? price : 0.00f,
                        Type = product.Type,
                        ShopId = 1  
                    };
                    newProduct.ImageUrl = product.ProductImage != null
                        ? await _imageHandler.UploadImage(product.Name, product.ProductImage)
                        : "";
                    await _quickSnaxContext.Products.AddAsync(newProduct);
                    var result = await _quickSnaxContext.SaveChangesAsync();
                    return result == 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
        }

        
        
        
        
        public async Task<bool> UpdateProduct(Product product,IFormFile imageFile)
        {
            try
            {
                product.ImageUrl = imageFile != null
                    ? await _imageHandler.UploadImage(product.Name, imageFile)
                    : "";
                _quickSnaxContext.Entry(product).State = EntityState.Modified;
                return await _quickSnaxContext.SaveChangesAsync() == 1;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
            
            
        }

        public async Task<Product> Find(int id)
        {
            return await _quickSnaxContext.Products.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Product>> Find(string name)
        {
            return await _quickSnaxContext.Products.Where(x => x.Name.Contains(name)).ToListAsync();
        }

    public async Task<bool> Delete(int id)
        {
           var product =  Find(id).Result;
           if (product == null)
           {
               return false;
           }
           _quickSnaxContext.Products.Remove(product);
           return await _quickSnaxContext.SaveChangesAsync() == 1;
        }
        
    }
}